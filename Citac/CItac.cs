﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Citac {
    class Citac {
        public int limit = 4;
        public int pocitadlo = 0;
        public int Zvys() {
            return pocitadlo +=1;
        }
        public delegate void Udalost();
        public event Udalost PrekrocenaMezEvent;
        static void Vypis() {
            Console.WriteLine("Mez prekrocena");
        }

        static void Main(string[] args) {
            Citac c = new Citac();
            c.PrekrocenaMezEvent = Vypis;

            c.Zvys();
            Console.WriteLine("Prvni volani metody");
            System.Threading.Thread.Sleep(1000);
            c.Zvys();
            Console.WriteLine("Druhe volani metody");
            System.Threading.Thread.Sleep(1000);
            c.Zvys();
            Console.WriteLine("Treti volani metody");
            System.Threading.Thread.Sleep(1000);
            c.Zvys();
            Console.WriteLine("Ctvrte volani metody");
            System.Threading.Thread.Sleep(1000);
            if (c.PrekrocenaMezEvent != null) {
                Vypis();
            }
        }
    }
}
