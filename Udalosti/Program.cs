﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Udalosti {
    class Program {
        static int counter = 0;
        static string displayString = "Tento text se objevuje pismenko po pismenku. ";
        static void Main(string[] args) {
            Timer casovac = new Timer(50);
            casovac.Elapsed += new ElapsedEventHandler(WriteChar);
            casovac.Start();
            System.Threading.Thread.Sleep(200);
            Console.ReadKey();
        }
        static void WriteChar (object source, ElapsedEventArgs e) {
            Console.Write(displayString[counter++ % displayString.Length]);
        }
    }
}
