﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegaty {
    delegate double ProcessDelegate(double a, double b);

    class Program {
        //delegate double ProcessDelegate(double a, double b);
        static double Multiply(double a, double b) {
            return a * b;
        }
        static double Divide(double a, double b) {
            return a / b;
        }
        static void Main(string[] args) {
            ProcessDelegate process;
            Console.WriteLine("Vloz prvni cislo");
            double a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Vloz druhe cislo");
            double b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter M to multiply or D to divide:");
            string input = Console.ReadLine();
            if (input == "M")
                process = new ProcessDelegate(Multiply);
            else
                process = new ProcessDelegate(Divide);

            Console.WriteLine($"Result: {process(a, b)}");
            Console.ReadKey();
        }
    }
}
