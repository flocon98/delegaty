﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegaty_void {
    delegate void ObchodniZastupce();
    class Firma {
        static void StrojTech() => 
            Console.WriteLine("Karel dnes bude zastupovat firmu StrojTech");
        
        static void BořilASyn() {
            Console.WriteLine("Karel dnes bude zastupovat firmu Bořil a syn");
        }

        static event ObchodniZastupce prichodOZ;  //handler
        static void obchodak_zdravi() {
            Console.WriteLine("Dobry den, jsem Karel a jsem obchodni zastupce.");
        }
        static void obchodak_nabizi() {
            Console.WriteLine("Mate zajem o nase produkty?");
        }

        static void Main(string[] args) {
            ObchodniZastupce karel;
            prichodOZ += obchodak_zdravi;
            prichodOZ += obchodak_nabizi;

            Console.WriteLine("Kterou firmu má nas delegát dnes zastupovat?\n (S = Strojtech, \n B = Bořil a syn, \n O = obě");
            string input = Console.ReadLine();
            if (input == "O") {
                karel = new ObchodniZastupce(StrojTech);
                karel += new ObchodniZastupce(BořilASyn);
            }
            else if (input == "S") {
                karel = new ObchodniZastupce(StrojTech);
            }
            else {
                karel = new ObchodniZastupce(BořilASyn);
            }
            karel();

            System.Threading.Thread.Sleep(2000);
            if(prichodOZ!=null) {
                obchodak_zdravi();
                obchodak_nabizi();
            }

            Console.ReadKey();
        }
    }
}
